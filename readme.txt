1. put files to deploy directory
deploy/
├── config
│   ├── env.cfg
│   ├── sys_config.fex
│   ├── sys_partition_linux.fex
│   └── sys_partition_nor.fex
├── kernel
│   ├── arisc
│   ├── boot.img
│   ├── .sunivw1p1-evb.dtb.d.dtc.tmp
│   ├── .sunivw1p1-evb.dtb.dts
│   ├── sunxi.dtb
│   ├── uImage
│   ├── vmlinux.tar.bz2
│   └── zImage
├── rootfs
│   ├── rootfs.ext4
│   └── rootfs.squashfs
└── u-boot
    ├── boot0_nand_sunivw1p1.bin
    ├── boot0_sdcard_sunivw1p1.bin
    ├── boot0_spinor_sunivw1p1.bin
    └── u-boot-sunivw1p1.bin

2. run script to generate system image
