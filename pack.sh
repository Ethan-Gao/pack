#!/bin/sh
DEPLOY=$1
TOP=$PWD

echo $DEPLOY
echo $TOP

### UBOOT
UBOOT_FILES="u-boot-sunivw1p1.bin boot0_nand_sunivw1p1.bin\
	boot0_sdcard_sunivw1p1.bin boot0_spinor_sunivw1p1.bin"
UBOOT_DIR="$TOP/tools/pack/chips/sunivw1p1/bin"
cd $DEPLOY/u-boot
echo $PWD
cp -v $UBOOT_FILES $UBOOT_DIR
cp -v $UBOOT_DIR/u-boot-sunivw1p1.bin $UBOOT_DIR/u-boot-spinor-sunivw1p1.bin
cd $TOP
### KERNEL
KERNEL_FILES="arisc boot.img sunxi.dtb uImage vmlinux.tar.bz2 zImage"
KERNEL_DIR="$TOP/out/sunivw1p1/linux/common"
DTB_FILES=".sunivw1p1-evb.dtb.d.dtc.tmp .sunivw1p1-evb.dtb.dts"
DTB_DIR="$TOP/linux-3.10/arch/arm/boot/dts"
cd $DEPLOY/kernel
echo $PWD
mkdir -p $KERNEL_DIR $DTB_DIR
cp -v $KERNEL_FILES $KERNEL_DIR
cp -v $DTB_FILES $DTB_DIR
cd $TOP
### ROOTFS
ROOTFS_FILES="test-image-suniv.ext4 test-image-suniv.squashfs"
ROOTFS_DIR="$TOP/out/sunivw1p1/linux/common"
cd $DEPLOY
echo $PWD
mkdir -p $ROOTFS_DIR
cp -v $ROOTFS_FILES $ROOTFS_DIR
cd $TOP
### CONFIG
cd $TOP/deploy/config
echo $PWD
cp -v env.cfg $TOP/tools/pack/chips/sunivw1p1/configs/default/env.cfg
cp -v sys_config.fex $TOP/tools/pack/chips/sunivw1p1/configs/evb/sys_config.fex
cp -v sys_partition_nor.fex $TOP/tools/pack/chips/sunivw1p1/configs/default/sys_partition_nor.fex
cp -v sys_partition_linux.fex $TOP/tools/pack/chips/sunivw1p1/configs/default/sys_partition_linux.fex
cd $TOP
### PACK
PACK_DIR="$TOP/tools/pack"
cd $PACK_DIR
./pack -c sunivw1p1 -p linux -b evb -k linux-3.10 $@
cd $TOP

### IMAGE
echo "#######SDCARD IMAGE: "
ls $TOP/tools/pack/sunivw1p1_linux_evb_uart0.img
echo "#######SPINOR IMAGE: "
ls $TOP/tools/pack/out/full_img.fex
